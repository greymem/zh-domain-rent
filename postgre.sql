-- Table: public.participants

-- DROP TABLE public.participants;

CREATE TABLE public.participants
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT participants_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

COMMENT ON TABLE public.participants
    IS 'Users: landlords and tenants';
	

-- Table: public.estate

-- DROP TABLE public.estate;

CREATE TABLE public.estate
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    landlord_id integer NOT NULL,
    building_id integer,
    address character varying(1024) COLLATE pg_catalog."default" NOT NULL,
    estate_type estate_types,
    CONSTRAINT estate_pkey PRIMARY KEY (id),
    CONSTRAINT estate_apt_building_fk FOREIGN KEY (building_id)
        REFERENCES public.estate (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT estate_landlort_fk FOREIGN KEY (landlord_id)
        REFERENCES public.participants (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

COMMENT ON TABLE public.estate
    IS 'Private property of landlords. Whole building or just apartment. Set building_id when estate_type="apartment"';

COMMENT ON COLUMN public.estate.estate_type
    IS '"building" or "apartment"';

COMMENT ON CONSTRAINT estate_apt_building_fk ON public.estate
    IS 'Link apartment to building (if estate_type="apartment")';
COMMENT ON CONSTRAINT estate_landlort_fk ON public.estate
    IS 'Link estate to landlord';
	

-- Table: public.rental_contracts

-- DROP TABLE public.rental_contracts;

CREATE TABLE public.rental_contracts
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    estate_id integer NOT NULL,
    tenant_id integer NOT NULL,
    date_from date NOT NULL,
    date_to date,
    CONSTRAINT rental_contracts_pkey PRIMARY KEY (id),
    CONSTRAINT rental_contracts_estate_fk FOREIGN KEY (estate_id)
        REFERENCES public.estate (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT rental_contracts_tenant_fk FOREIGN KEY (tenant_id)
        REFERENCES public.participants (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

COMMENT ON TABLE public.rental_contracts
    IS 'All rent contracts';

COMMENT ON CONSTRAINT rental_contracts_estate_fk ON public.rental_contracts
    IS 'Link contract to estate (any kind, whole building or apartment)';
COMMENT ON CONSTRAINT rental_contracts_tenant_fk ON public.rental_contracts
    IS 'Link contracts to tenant';